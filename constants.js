/* constants.js
 *
 * This file is part of Run Android Emulators (gnome-shell-extension-runandroidemulators)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const GSETTINGS_ANDROID_SDK_PATH_KEY = 'android-sdk-path';
export const EXTENSION_TITLE = 'Run Android Emulators';
export const INITIALIZE_MENU_ITEMS_DELAY_IN_MS = 2000;
export const SYNC_MENU_ITEMS_INTERVAL_IN_MS = 30000;
